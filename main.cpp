#define BATCH
#define ENABLE_REFRESH

#include "QuGlobals.h"
#include "s3e.h"
#include "GuiAp.h"
#include "QuUtils.h"
#include "QuGameManager.h"


#include "Global.h"
#include "SuiAp.h"
#include "APMain.h"
#include "s3eKeyboard.h"
#include "QuMouse.h"

static const float F_DOUBLE_TAP_TIME_THRESHOLD = 200.0f; //double tapping
static const float F_TAP_DISTANCE_THRESHOLD = 0.015f;		//distance grace for a "tapping" gesture
static const float F_TAP_TIME_THRESHOLD = 200.0f; //ms grace for a "tapping" gesture
static const float F_SWIPE_THRESHOLD = 0.03f; //distance threshold for a "swiping" gesture
static const float MOUSE_MOVE_GRACE = 300; // ms backtracking on mouse gestures


GuiKeyType mapDirToGuiKeyP2(GDirectionalInput aInput)
{
	if(aInput & G_LEFT)
		return GuiKeyType('j');
	else if(aInput & G_UP)
		return GuiKeyType('i');
	else if(aInput & G_RIGHT)
		return GuiKeyType('l');
	else if(aInput & G_DOWN)
		return GuiKeyType('k');
	
	quAssert(false);
	return GuiKeyType(0);
}

GuiKeyType mapDirToGuiKeyP1(GDirectionalInput aInput)
{
	if(aInput & G_LEFT)
		return GUI_LEFT;
	else if(aInput & G_UP)
		return GUI_UP;
	else if(aInput & G_RIGHT)
		return GUI_RIGHT;
	else if(aInput & G_DOWN)
		return GUI_DOWN;
	
	quAssert(false);
	return GuiKeyType(0);
}

void ApMapKeys(std::vector<GuiKeyType>& vApMapper)
{
	//this should take AP keys as index have GUI keys as value
    vApMapper.resize(400);
	vApMapper[s3eKeyLeft] = GUI_LEFT;
	vApMapper[s3eKeyRight] = GUI_RIGHT;
	vApMapper[s3eKeyUp] = GUI_UP;
	vApMapper[s3eKeyDown] = GUI_DOWN;
	vApMapper[s3eKeySpace] = GuiKeyType(32);
	vApMapper[111] = GuiKeyType(92);
	vApMapper[s3eKeyEnter] = GUI_RETURN;
	vApMapper[s3eKeyT] = GuiKeyType('t');
	vApMapper[s3eKeyM] = GuiKeyType('m');
	vApMapper[s3eKeyP] = GuiKeyType('p');

	vApMapper[s3eKeyF2] = GUI_F2;
	vApMapper[s3eKeyF3] = GUI_F3;
	vApMapper[s3eKeyF4] = GUI_F4;
	vApMapper[s3eKeyF5] = GUI_F5;
	vApMapper[s3eKeyF6] = GUI_F6;
	vApMapper[s3eKeyF7] = GUI_F7;
	vApMapper[s3eKeyF8] = GUI_F8;
	vApMapper[s3eKeyF9] = GUI_F9;
	vApMapper[s3eKeyF10] = GUI_F10;

	vApMapper[s3eKeyAbsBSK] = GuiKeyType(32); //circle
	vApMapper[s3eKeyAbsOk] = GuiKeyType(32); //cross
	vApMapper[s3eKeyAbsGameC] = GuiKeyType('p'); //triangle pauses
	vApMapper[s3eKeyAbsGameD] = GuiKeyType(32); //square
}

unsigned GetTicks()
{
	return G_GAME_GLOBAL.getMillis();
}


class GenGuiManager : public QuBaseManager
{
	bool mInit;
	SP<GlobalController> pGl;
	SP<ApGraphicalInterface> pGraph;
	std::vector<GuiKeyType> vApMapper;
	QuAlgebraicScreenCoord mLastMouse;
	QuAlgebraicScreenCoord mDownMouse;
	int mLastTime;
	Gui::Point mTotalMouse;
	bool mMouseDown;
	std::set<int> mButtonsDown;

	//stupid thing lol.. cuz a leak is better than a crash
	QuStupidPointer<QuBaseManager> * mSelf;
public:
	GenGuiManager():mInit(false),mLastMouse(G_SCREEN_WIDTH,G_SCREEN_HEIGHT),mMouseDown(false)
	{
		
	}
	~GenGuiManager()
	{	
		
	}

	void screenResize(int w, int h)
	{
		pGl = NULL;
	}

	void guiInit()
	{
		srand( (unsigned)time( NULL ));
		try
		{
			ProgramInfo inf = GetProgramInfo();
			Rectangle sBound = Rectangle(Point(0, 0), inf.szScreenRez);
			//Rectangle sBound = Rectangle(Point(0, 0), Point(G_SCREEN_WIDTH,G_SCREEN_HEIGHT));
			pGraph = new ApGraphicalInterface(sBound.sz);
			SP< GraphicalInterface<Index> > pGr = new SimpleGraphicalInterface<ApImage*>(pGraph);
			SP<ApSoundInterface> pSound = new ApSoundInterface();
			SP< SoundInterface<Index> > pSndMng = new SimpleSoundInterface<ApSoundObject> (pSound);
			//pGr->DrawRectangle(sBound, Color(0,0,0), true);
			ProgramEngine pe(new EmptyEvent(), pGr, pSndMng, new IoWriter(),inf.szScreenRez);
			pGl = GetGlobalController(pe);
			std::cout << "global controller constructed" << std::endl;
			ApMapKeys(vApMapper);
			Point pMousePos = Point(0,0);
		}
		catch(MyException& me)
		{
			std::cout << "exceptin in main setup: " << me.GetDescription(true) << "\n";
		}
		catch(...)
		{
			std::cout << "Unknown error!\n";
		}
		
	}
	virtual void initialize()
	{
		//stupid hack to avoid crash
		mSelf = new QuStupidPointer<QuBaseManager>();
		*mSelf = G_GAME_GLOBAL.getManager();

		glClearColor(0,0,0,1);
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
		guiInit();
		mInit = true;
	}
	virtual void update()
	{
		#ifdef BATCH
		//clearScreen();
		#endif
		runGui();
		pGraph->draw();
		
	}
	virtual void keyDown(int key)
	{ 
		if(key == s3eKeyAbsBSK)
			pGl->KeyDown(GuiKeyType('p'));
		else
			pGl->KeyDown(vApMapper[key]); 
	}

	virtual void keyUp(int key){ pGl->KeyUp(vApMapper[key]); }
	virtual void singleDown(int button, QuScreenCoord crd)
	{
		mMouseDown = true;
		QuScreenCoord conv = pGraph->convertScreenToCameraCoord(crd);
		conv.rotate(pGraph->getCurrentCameraRotation());
		conv.reflect(G_SCREEN_REFLECT_VERTICAL);
		mLastMouse = conv;
		mDownMouse = conv;

		if(G_GAME_GLOBAL.getMillis() - mLastTime < F_DOUBLE_TAP_TIME_THRESHOLD)
			pGl->DoubleClick();

		mLastTime = G_GAME_GLOBAL.getMillis();

		QuVector2<int> t = conv.getIPosition();
		pGl->MouseDown(Gui::Point(t.x,t.y));
	}
	virtual void singleUp(int button, QuScreenCoord crd)
	{
		mMouseDown = false;
		QuScreenCoord conv = pGraph->convertScreenToCameraCoord(crd);
		conv.rotate(pGraph->getCurrentCameraRotation());
		conv.reflect(G_SCREEN_REFLECT_VERTICAL);
		if(G_GAME_GLOBAL.getMillis() - mLastTime < F_TAP_TIME_THRESHOLD)
		{
			//std::cout << conv.getFChange(mLastMouse).magnitude() << std::endl;
			if(conv.getFChange(mDownMouse).magnitude() < F_TAP_DISTANCE_THRESHOLD)
			{
				//sendButton(GuiKeyType(' '));
			}
		}
		pGl->MouseUp();
	}
	virtual void singleMotion(QuScreenCoord crd)
	{
		if(!mMouseDown) return;
		QuScreenCoord conv = pGraph->convertScreenToCameraCoord(crd);
		conv.rotate(pGraph->getCurrentCameraRotation());
		conv.reflect(G_SCREEN_REFLECT_VERTICAL);
		QuAlgebraicScreenCoord change = QuAlgebraicScreenCoord(conv) - mLastMouse;
		mLastMouse = conv;
		//QuVector2<int> t = change.getIPosition();
		//std::cout << t.x << " " << t.x << std::endl;
		//mTotalMouse += Gui::Point(t.x,t.y);
		//pGl->MouseMove(mTotalMouse);


		QuVector2<int> t = conv.getIPosition();
		pGl->MouseMove(Gui::Point(t.x,t.y));
	}

	void sendButton(GuiKeyType key)
	{
		pGl->KeyDown(key);
		pGl->KeyUp(key);
	}
	void sendClick(QuVector2<int> p)
	{
		pGl->MouseClick(Gui::Point(p.x,p.y));
	}
	void multiTouch(int button, bool state,  QuScreenCoord crd)
	{

		//if(false)//mButtonsDown.size() == 3 && !state)
			//pGl->KeyDown(GuiKeyType('p'));
		if(mButtonsDown.size() == 4 && !state)
			pGl->KeyDown(GuiKeyType('\\'));
		else
		{
			if(!state && button != 0)
			{
				pGl->Fire();
				
				/*
				QuScreenCoord conv = pGraph->convertScreenToCameraCoord(crd);
				conv.rotate(pGraph->getCurrentCameraRotation());
				conv.reflect(G_SCREEN_REFLECT_VERTICAL);
				QuVector2<int> t = conv.getIPosition();
				pGl->MouseDown(Gui::Point(t.x,t.y));
				pGl->MouseUp();*/
			}
		}

		if(state)
			mButtonsDown.insert(button);
		else
			mButtonsDown.erase(button);

		
	}

	void multiMotion(int button,  QuScreenCoord crd)
	{
	}

	void clearScreen()
	{
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	}
	void runGui()
	{
		try{
			pGl->Update();
		}
		catch(...){
			std::cout << "exception" << std::endl;
			s3eDeviceExit();
		}	
	}



};

int main()
{
	
	QuStupidPointer<QuGlobalSettings> s = new QuGlobalTypeSettings<QuApRawSoundManager<22050> ,QuImageManager,ApFileManager>();
	s->mRefresh = false;
#ifndef BATCH
	s->mRefresh = false;
#endif
	s->mFramerate = 1000/30;
	INITIALIZE_GAME<GenGuiManager>(s);
	//return APMain(new GenGuiManager());
}

